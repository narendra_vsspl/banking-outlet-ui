import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/modules/auth/authentication.service';
import { SyncDataService } from 'src/app/services/sync-data.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { fade, expandCollapse } from 'src/app/animations';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    animations: [fade, expandCollapse]
})
export class HeaderComponent implements OnInit {

    assets: any;
    isUserLogin: boolean;
    userData: any;
    isHome = true;
    showSpinner = false;
    masterData: any;
    stockboardData: any[];
    model: any;
    user: any;
    transactionTypes: any = [{ value: 'SALE', name: 'SALE' }, { value: 'PURCHASE', name: 'PURCHASE' }];
    connects: any[] = [];
    isTypeChange: boolean = true;
    filteredOptions: Observable<any[]>;
    receiverData: any;
    productData: any[] = [];

    lastSyncTime: any;
    routerHeading: string;
    transactions: any;

    constructor(private router: Router,
                private authService: AuthenticationService,
                private syncDataService: SyncDataService,
                private spinnerService: SpinnerService,
                private snackbar: MatSnackBar) {
                    this.spinnerService.spinnerStatus.subscribe(val => {
                      this.showSpinner = val;
                    });
                }

    ngOnInit() {
        this.isUserLogin = false;
        this.routerHeading = 'Create Transaction';
        this.authService.updateData.on('user changed', () => {
            this.isUserLogin = localStorage.getItem('userInfo') === null ? false : true;
            this.userData = this.authService.getUserData();
        });
        this.syncDataService.backBtn.subscribe((flag) => {
            if (flag === 'Home') {
                this.isHome = true;
                if (this.router.url.includes('stockShifting')) {
                    this.routerHeading = 'Stock Shifting';
                    this.stockShifting();
                } else {
                    this.routerHeading = 'Create Transaction';
                }
            } else {
                this.isHome = false;
                this.routerHeading = flag;
            }
        });
    }

    // getRouterHeading(url) {
    //     let title: string;
    //     if (url.includes('createTransction')) {
    //         title = 'Create Transaction';
    //     } else if (url.includes('stockShifting')) {
    //         title = 'Stock Shifting';
    //     } else if (url.includes('sync')) {
    //         title = 'Sync Transactions';
    //     } else if (url.includes('stockboard')) {
    //         title = 'Stockboard';
    //     }
    //     return title;
    // }

    back() {
        this.syncDataService.backBtn.emit('Home');
        this.isHome = true;
    }

    createTransaction() {
        this.router.navigate(['app/create']);
        this.routerHeading = 'Create Transaction';
    }

    stockShifting() {
        this.router.navigate(['app/stockShifting']);
        this.routerHeading = 'Stock Shifting';
    }

    sync() {
        this.router.navigate(['app/sync']);
        this.routerHeading = 'Sync Transactions';
    }

    connect() {
        this.router.navigate(['app/connects']);
        this.routerHeading = 'My Connects';
    }
    myTransactions() {
        this.router.navigate(['app/mytransactions']);
        this.routerHeading = 'My Transactions';
    }
    stockBoard() {
        this.router.navigate(['app/stockboard']);
        this.routerHeading = 'Stockboard';
    }
    syncMasterData() {
        let pendingTransactions = false;
        this.user = this.authService.getUserData();
        this.spinnerService.show();
        this.transactions = JSON.parse(localStorage.getItem('mulkanoorTransactions'));
        this.transactions.transactions.forEach(t => {
            if (!t.isSyncSuccess) {
                pendingTransactions = true;
                return;
            }
        });
        if (!pendingTransactions) {
            this.authService.getOutletMasterData().subscribe(res => {
                if (res.code === 200) {
                    localStorage.setItem('mulkanoorMasterData', JSON.stringify(res.data));
                    this.masterData = res.data;
                    this.connects = this.masterData.connects;
                    // this.connects = this.connects.concat(this.masterData.bizContacts);
                    this.productData = this.masterData.products;
                    this.lastSyncTime = this.masterData.lastMasterSyncTs;
                    this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
    
                    if (this.masterData && this.masterData.stockData) {
                        this.stockboardData = new Array();
                        // tslint:disable-next-line: forin
                        for (let i in this.masterData.stockData) {
                            this.stockboardData.push(this.masterData.stockData[i]);
                        }
                    }
                    this.spinnerService.hide();
                    // this.router.navigate(['app/stockboard']);
                    // this.routerHeading = 'Stockboard';
                    this.snackbar.open('Data fetch successfully', 'ok', { duration: 4000 });
                } else {
                    this.spinnerService.hide();
                    this.snackbar.open('Data sync failed', 'ok', { duration: 4000 });
                }
            },
            (err => {
                this.snackbar.open('Error: Internal Server Error, please check your internet connection', 'X', { duration: 4000 });
                this.spinnerService.hide();
              }));
        } else {
            this.spinnerService.hide();
            this.snackbar.open('Please sync all transactions', 'X', { duration: 4000 });
            this.router.navigate(['app/sync']);
            this.routerHeading = 'Sync Transactions';
        }
    }


    logOut() {
        this.routerHeading = 'Create Transaction';
        this.router.navigate(['pages/signin']);
        localStorage.clear();
        setTimeout(() => {
            this.authService.notificationForUserData();
        }, 300);
    }

}
