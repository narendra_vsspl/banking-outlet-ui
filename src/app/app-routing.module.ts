import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthModule } from './modules/auth/auth.module';
import { SigninComponent } from './modules/auth/signin/signin.component';
import { SyncDataComponent } from './modules/sync/sync-data/sync-data.component';
import { CreateTransactionComponent } from './modules/transactions/create-transaction/create-transaction.component';
import { AuthGuard } from './auth/auth.guard';
import { AddProductComponent } from './modules/transactions/add-product/add-product.component';
import { MyTransactionsComponent } from './modules/transactions/my-transactions/my-transactions.component';
import { StockboardComponent } from './modules/transactions/stockboard/stockboard.component';
import { StockShiftingComponent } from './modules/transactions/stock-shifting/stock-shifting.component';



const routes: Routes = [
  {
    path: 'app',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'create',
        component: CreateTransactionComponent,
      },
      {
        path: 'stockShifting',
        component: StockShiftingComponent,
      },
      {
        path: 'sync',
        component: SyncDataComponent,
      },
      {
        path: 'create/add_products',
        component: AddProductComponent,
      },
      {
        path: 'mytransactions',
        component: MyTransactionsComponent,
      },
      {
        path: 'stockboard',
        component: StockboardComponent,
      }
    ]
  },

  {
    path: 'pages',
    children: [
      {
        path: 'signin',
        component: SigninComponent
      }
    ]
  },

  {
    path: '',
    redirectTo: '/app/create',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/app/create',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    AuthModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
