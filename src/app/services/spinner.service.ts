import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  private $spinnerStatus = new Subject<boolean>();

  constructor() { }

  public get spinnerStatus(): Subject<boolean> {
    return this.$spinnerStatus;
  }

  public show(): void {
    this.$spinnerStatus.next(true);
  }

  public hide(): void {
    this.$spinnerStatus.next(false);
  }
}
