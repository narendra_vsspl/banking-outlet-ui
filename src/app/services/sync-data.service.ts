import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})

export class SyncDataService {

  @Output() backBtn = new EventEmitter();
  userInfo: any;



  constructor(private http: HttpClient) { }

  getHttpOptions() {
    const userInfo = JSON.parse(localStorage.getItem('userInfo'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'username': userInfo.userName,
        'userid': `${userInfo.userId}`,
        'Authorization': 'Bearer ' + userInfo.token
      }), body: ''
    };
    return httpOptions;
  }

  syncToKalgugi(syncData): Observable<any> {
    const httpOptions = this.getHttpOptions();
    const requestUrl = `${environment.domain}/bank/transaction-outlet/outletdatasink`;
    return this.http.post(requestUrl, syncData, httpOptions).pipe(map(response => {
      return response;
    }));
  }

  getThirdParty(thirdPartyData): Observable<any> {
    const requestUrl = `${environment.restBaseUrl}/outletThirdParties`;
    return this.http.put(requestUrl, thirdPartyData).pipe(map(response => {
      return response;
    }));
  }
}
