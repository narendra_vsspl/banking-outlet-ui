import { Injectable, Output } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter } from 'events';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  userInfo: any;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'username': '',
      'num': '465',
      'userid': '',
      'Authorization': ''
    }), body: ''
  };
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  @Output() updateData = new EventEmitter();

  @Output() submitProduct = new EventEmitter();

  constructor(private http: HttpClient) {
    if (localStorage.getItem('userInfo') !== null) {
      this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('userInfo'));
    } else {
      this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('userInfo'));
    }
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
}

public notificationForUserData(message: string = 'user changed') {
  this.updateData.emit(message);
}

  getUserData() {
    const userData = localStorage.getItem('userInfo');
    if (userData) {
      this.userInfo = JSON.parse(userData);
      // console.log('adsadsa', this.userInfo);
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'username': this.userInfo.userName,
          'num': '465',
          'userid': `${this.userInfo.userId}`,
          'Authorization': 'Bearer ' + this.userInfo.token
        }), body: ''
      };
    }

    return this.userInfo;
  }


  storeUserLoginData(data: any) {
    this.currentUserSubject.next(data);
    localStorage.setItem('userInfo', JSON.stringify(data));
  }


  public login(request: any): Observable<any> {
    const url = `${environment.domain}/bank/login`;

    return this.http.post(url, request).pipe(map(data => {
      const response = data;
      setTimeout(() => {
      this.notificationForUserData();
      }, 300);
      return  response;
    }));
  }

  public getOutletMasterData(): Observable<any> {
    const url = `${environment.domain}/bank/transaction-outlet/outletMasterData`;
    return this.http.get<any>(url, this.httpOptions);

  }

  public notifyProductData(data: any) {
    this.submitProduct.emit('Product Data', JSON.stringify(data));
  }
}
