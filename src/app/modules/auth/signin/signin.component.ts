import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from '../authentication.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  isMobileChecked = true;
  hidePassword = true;
  mobileRequired = true;
  emailRequired = true;
  loginRequest: any;

  loginForm: FormGroup;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService) {
      this.loginForm = new FormGroup({
        email: new FormControl(''),
        mobileNo: new FormControl('', Validators.required),
        pwd: new FormControl('', Validators.required),
        isMobile: new FormControl('mobile')
      });
    }

  ngOnInit() {}
  login() {
      let mobile = this.loginForm.value.mobileNo;
      let mobilePattern = /^[0-9]{10}$/;
      mobile = mobilePattern.test(mobile) ? '+91' + mobile : mobile;
      this.loginRequest = {
        username : this.isMobileChecked ? mobile : this.loginForm.value.email,
        password : this.loginForm.value.pwd
      };
      this.spinnerService.show();
      this.authService.login(this.loginRequest)
        .subscribe( res => {
          if (res.code === 201 && res.data) {
            res.data = res.data;
            this.router.navigate(['app/create']);
            this.authService.storeUserLoginData(res.data);
            this.spinnerService.hide();
          } else if (res.code === 401) {
            this.showMessage('Incorrect password or user name');
            this.loginForm.get('pwd').patchValue('');
            this.spinnerService.hide();
          } else {
            this.loginForm.get('pwd').patchValue('');
            this.loginForm.get('email').patchValue('');
            this.loginForm.get('mobileNo').patchValue('');

            this.showMessage('This account is not registered with us ' + mobile);
            this.spinnerService.hide();
          }

        },
        (err => {
          this.snackBar.open('Internal Server Error, please try again after some time', 'X', { duration: 4000 });
          this.spinnerService.hide();
        }));
    }

    showMessage(msg: string) {
      this.snackBar.open(msg, 'OK', {
        duration: 5000
      });
    }

}
