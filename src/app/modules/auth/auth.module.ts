import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { SharedUiModule } from 'src/app/shared/components/shared-ui-module/shared-ui-module.module';


@NgModule({
  declarations: [SigninComponent],
  imports: [
    CommonModule,
    SharedUiModule
  ]
})
export class AuthModule { }
