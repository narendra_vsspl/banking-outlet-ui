import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiModule } from 'src/app/shared/components/shared-ui-module/shared-ui-module.module';
import { SyncDataComponent } from './sync-data/sync-data.component';



@NgModule({
  declarations: [SyncDataComponent],
  imports: [
    CommonModule,
    SharedUiModule
  ]
})
export class SyncModule { }
