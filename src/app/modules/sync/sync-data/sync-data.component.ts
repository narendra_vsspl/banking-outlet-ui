import { Component, OnInit } from '@angular/core';
import { SyncDataService } from 'src/app/services/sync-data.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { MatSnackBar } from '@angular/material';
import { CheckboxControlValueAccessor } from '@angular/forms';

@Component({
    selector: 'app-sync-data',
    templateUrl: './sync-data.component.html',
    styleUrls: ['./sync-data.component.scss']
})
export class SyncDataComponent implements OnInit {

    syncData: any;
    response: any;
    lastSyncData: any;
    pendingCount: number;
    dates: any = [];
    bizContactList = [];
    thirdParty: any;
    responseThirdPartyData: any;
    businessContactObj = {
        bizContacts: []
    };
    newSyncData: {
        transactions: any,
        outletProfileKey: ''
    };

    thirdPartyData: any;

    constructor(
        private syncDatas: SyncDataService,
        private spinnerService: SpinnerService,
        private snackbar: MatSnackBar
    ) { }

    ngOnInit() {
        this.pendingCount = 0;
        if (localStorage.getItem('mulkanoorTransactions')) {
            this.syncData = JSON.parse(localStorage.getItem('mulkanoorTransactions'));
            this.syncData.transactions.forEach(transaction => {
                if (!transaction.isSyncSuccess) {
                    this.pendingCount += 1;
                }
            });

            this.syncData.stockShift.forEach(transaction => {
                if (!transaction.isSyncSuccess) {
                    this.pendingCount += 1;
                }
            });

            this.dates = this.syncData.transactions.reduce((unique, item) => {
                return unique.includes(new Date(item.CT).toLocaleDateString()) ? unique : [...unique, new Date(item.CT).toLocaleDateString()];
            }, []);
        }
        if (localStorage.getItem('mulkanoorLastSyncData')) {
            this.response = JSON.parse(localStorage.getItem('mulkanoorLastSyncData'));
        }
        this.thirdPartyData = JSON.parse(localStorage.getItem('businessContacts'));
        this.businessContactObj.bizContacts = this.thirdPartyData;
    }
    thirdPartyObj;
    thirdPartiesCreated;


    sync() {
        this.spinnerService.show();
        if (this.businessContactObj && this.businessContactObj.bizContacts && this.businessContactObj.bizContacts.length > 0) {
            this.syncDatas.getThirdParty(this.businessContactObj).subscribe(response => {

                if (response.code === 201) {
                    var data = JSON.parse(response.data);
                    this.thirdPartiesCreated = data.created;

                    for (this.thirdPartyObj of this.thirdPartiesCreated) {
                        for (var i = 0; i < this.syncData.transactions.length; i++) {
                            if (this.thirdPartyObj.mobileNo === this.syncData.transactions[i].receiverMobileNo) {
                                this.syncData.transactions[i].receiverProfileKey = this.thirdPartyObj.thirdPartyProfileKey;;
                                this.syncData.transactions[i].receiverId = this.thirdPartyObj.thirdPartyBusinessKey;
                                this.syncData.transactions[i].receiverBizId = this.thirdPartyObj.thirdPartyBusinessKey;
                                this.syncData.transactions[i].thirdPartyTransaction = true;
                            }
                        }
                    }
                    // Sync transactions with Kalgudi with updated third parties (business contacts)
                    this.syncToKalgudi();
                } else {
                    this.spinnerService.hide();
                    this.snackbar.open('Something went wrong, please try again', 'ok', { duration: 4000 });
                }
            },
            (err => {
                this.snackbar.open('Internal Server Error, please check your internet connection', 'X', { duration: 4000 });
                this.spinnerService.hide();
              }));
        } else {
            // Sync transactions with Kalgudi
            this.syncToKalgudi();
        }


    }

    syncToKalgudi() {
        this.newSyncData = {
            transactions: [],
            outletProfileKey: ''
        };
        this.spinnerService.show();
        this.removeSuccessData();//this.syncData
        this.syncDatas.syncToKalgugi(this.newSyncData).subscribe(res => {
            if (res.code === 200) {
                this.spinnerService.hide();

                localStorage.setItem('mulkanoorLastSyncData', JSON.stringify(res.data));
                this.response = res.data;
                this.syncData.transactions.forEach(i => {
                    this.response.success.forEach(t => {
                        if (!i.isSyncSuccess && t === i.transactionId) {
                            i.isSyncSuccess = true;
                            this.pendingCount -= 1;
                            // this.syncData.transactions.splice(i);
                            localStorage.setItem('mulkanoorTransactions', JSON.stringify(this.syncData));
                            this.spinnerService.hide();
                            this.snackbar.open('Data sync successfully', 'ok', { duration: 4000 });
                        }
                    });

                    this.response.failed.forEach(t => {
                        if (!i.isSyncFailed && t === i.transactionId) {
                            i.isSyncFailed = true;
                            // this.syncData.transactions.splice(i);
                            localStorage.setItem('mulkanoorTransactions', JSON.stringify(this.syncData));
                            this.spinnerService.hide();
                            this.snackbar.open('Data sync failed', 'ok', { duration: 4000 });
                        }
                    });


                });
                this.removingTransactionDateWise();
                this.spinnerService.hide();

            } else {
                this.spinnerService.hide();
                this.snackbar.open('Something went wrong, please try again', 'ok', { duration: 4000 });
            }
        },
        (err => {
            this.snackbar.open('Error: Internal Server Error, please check your internet connection', 'X', { duration: 4000 });
            this.spinnerService.hide();
          }));
    }

    /* Removing Transactions That are successFull except tody transactions */
    removingTransactionDateWise() {
        this.syncData.transactions = this.syncData.transactions.filter(this.checkCondition);
        localStorage.removeItem('mulkanoorTransactions');
        localStorage.setItem('mulkanoorTransactions', JSON.stringify(this.syncData));
        this.syncData = JSON.parse(localStorage.getItem('mulkanoorTransactions'));
        this.dates = [];
        this.dates = this.syncData.transactions.reduce((unique, item) => {
            return unique.includes(new Date(item.CT).toLocaleDateString()) ? unique : [...unique, new Date(item.CT).toLocaleDateString()];
        }, []);
    }

    checkCondition({ CT: cts, isSyncSuccess: isSuccess }) {
        const cts1 = new Date(cts).toLocaleDateString();
        const today = new Date().toLocaleDateString();
        return ((cts1 === today) && isSuccess);
    }

    compareTwoDates(ctsA, ctsB) {
        const cts1 = ctsA;
        const cts2 = new Date(ctsB).toLocaleDateString();
        return (cts1 === cts2);
    }

    /*Remove success data */
    removeSuccessData(){
             this.syncData.transactions.forEach(i  => {  
             if(!i.isSyncSuccess){
               this.newSyncData.transactions.push(i);
             }
        });
          this.newSyncData.outletProfileKey = this.syncData.outletProfileKey;
        //   console.log("new Sync data",this.newSyncData);
    }
}