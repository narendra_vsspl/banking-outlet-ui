import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiModule } from 'src/app/shared/components/shared-ui-module/shared-ui-module.module';
import { CreateTransactionComponent } from './create-transaction/create-transaction.component';
import { AddProductComponent } from './add-product/add-product.component';
import { SalesComponent } from './sales/sales.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AddSellerComponent } from './add-seller/add-seller.component';
import { MyTransactionsComponent } from './my-transactions/my-transactions.component';
import { StockboardComponent } from './stockboard/stockboard.component';
import { StockShiftingComponent } from './stock-shifting/stock-shifting.component';
import { AddProductStockComponent } from './add-product-stock/add-product-stock.component';



@NgModule({
  declarations: [CreateTransactionComponent, SalesComponent, AddProductComponent, MyTransactionsComponent, AddSellerComponent, StockboardComponent, StockShiftingComponent, AddProductStockComponent],
  imports: [
    CommonModule,
    SharedUiModule
  ]
})
export class TransactionsModule { }
