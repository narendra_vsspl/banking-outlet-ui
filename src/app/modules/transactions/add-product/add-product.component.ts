import { Component, OnInit, OnChanges, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { AuthenticationService } from '../../auth/authentication.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SyncDataService } from 'src/app/services/sync-data.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit, OnChanges {

  @Output() addProduct = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Input() productData;
  @Input() transactionType: string;
  // productData: any;
  unitsList: any[] = [];
  masterData: any;
  validateUnitPrice: any;
  conversionFactor: number;
  msg: any;
  // productCtrl = new FormControl();
  filtredProduct: Observable<any[]>;
  invalidUnitPrice = false;
  isProductSelect = true;
  isUnitChange = true;
  isQuantityChange = true;
  validationQuantity: boolean;
  stock: any;
  productForm = new FormGroup({
    productName: new FormControl('', Validators.required),
    productId: new FormControl(''),
    units: new FormControl('', Validators.required),
    quantity: new FormControl(0, [Validators.required, Validators.min(1)]),
    unitPrice: new FormControl(0.0, [Validators.required, Validators.min(1)]),
    amount: new FormControl(0.0),
    varietyId: new FormControl(''),
    commodityId: new FormControl(''),
    commodityName: new FormControl(''),
    varietyName: new FormControl(''),
    qualifier: new FormControl(''),
    imageUrl: new FormControl(''),
    baseUnitId: new FormControl(''),
    baseUnitName: new FormControl(''),
  });
  constructor(private snackbar: MatSnackBar, private syncDataService: SyncDataService) { }

  constructForm(item: string) {
    if (item === 'PRODUCT') {
      this.isProductSelect = false;
    } else if (item === 'UNIT') {
      this.isUnitChange = false;
    } else if (item === 'QTY') {
      this.isQuantityChange = false;
    }
  }
  ngOnInit() {
    if (localStorage.getItem('mulkanoorMasterData')) {
      const data = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
      this.productData = data.products;
    }
    // this.productData = this.masterData;

    this.filtredProduct = this.productForm.get('productName').valueChanges.pipe(startWith(''), map(value => this._filtredProducts(value)));

    this.productForm.get('quantity').valueChanges.subscribe(value => {
      if (value > 0) {
        const amounts = value * this.productForm.value.unitPrice;
        this.productForm.get('amount').setValue(amounts);
      }
    });
    this.validationQuantity = false;

    this.productForm.get('unitPrice').valueChanges.subscribe(value => {
      if (value > 0) {
        const amounts = value * this.productForm.value.quantity;
        this.productForm.get('amount').setValue(amounts);
      }
    });
  }

  ngOnChanges(): void {
    this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
    this.productForm.get('quantity').valueChanges.subscribe(value => {
      this.validationQuantity = false;
      if (this.masterData && this.masterData.stockData && this.transactionType === 'SALE') {
        // tslint:disable-next-line: forin
        for (let i in this.masterData.stockData) {
          if (this.productForm.get('productId').value === this.masterData.stockData[i].productId)  {
            const unitFactor = this.getConversionFactor(this.productForm.get('units').value.unitName);
            if ((value * unitFactor) > this.masterData.stockData[i].closingBalance) {
              // this.snackbar.open('Product quantity not exceed ' + this.masterData.stockData[i].closingBalance, 'ok', { duration: 4000 });
              this.validationQuantity = true;
              this.stock = this.masterData.stockData[i].closingBalance / unitFactor;
            } else {
              this.validationQuantity = false;
            }
          }
        }
      }
    });
  }
  private _filtredProducts(value: string): any[] {
    if (value !== null) {
      const filteredValue = value.toLocaleLowerCase();
      const productSort = this.productData.filter(product => product.productName.toLocaleLowerCase().includes(filteredValue) || product.commodityName.toLocaleLowerCase().includes(filteredValue));
      if (productSort.length) {
        this.productForm.patchValue({
          productId: productSort[0].productId,
          varietyId: productSort[0].varietyId,
          commodityId: productSort[0].commodityId,
          commodityName: productSort[0].commodityName,
          varietyName: productSort[0].varietyName,
          qualifier: '',
          imageUrl: productSort[0].imageUrl,
          baseUnitId: productSort[0].baseUnitId,
          baseUnitName: productSort[0].baseUnitName,
        });
        this.unitsList = productSort[0].listofUnits;

      }
      this.constructForm('PRODUCT');
      return productSort;
    }
  }
  submitProduct(formDirective: FormGroupDirective) {
    this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
    this.validateUnitPrice = this.productForm.get('units').value;
    if (this.masterData && this.masterData.productsMSP && this.masterData.productsMSP.length) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.masterData.productsMSP.length; i++) {
        if (this.productForm.get('productId').value === this.masterData.productsMSP[i].productId) {
          // if (this.validateUnitPrice.unitName === 'quintal') {
          //    this.conversionFactor = 100;
          // }
          // if (this.validateUnitPrice.unitName === 'ton') {
          //   this.conversionFactor = 1000;
          // }
          // if (this.validateUnitPrice.unitName === 'kg') {
          //   this.conversionFactor = 1;
          // }
          this.conversionFactor = this.getConversionFactor(this.validateUnitPrice.unitName);
          if (this.productForm.get('unitPrice').value >= this.masterData.productsMSP[i].pricePerUnit * this.conversionFactor) {
            this.addProducts(formDirective);
            break;
          } else {
            this.msg = 'Rate should not less than '.concat((this.masterData.productsMSP[i].pricePerUnit * this.conversionFactor).toString());
            this.snackbar.open(this.msg, 'ok', { duration: 4000 });
            break;
          }
        } else {
          this.addProducts(formDirective);
          break;
        }

      }
    } else {
      this.addProducts(formDirective);
    }

  }

  getConversionFactor(value) {
    return this.productForm.get('units').value.conversion;
  }

  addProducts(formDirective) {
    this.addProduct.emit(this.productForm.value);
    this.snackbar.open('Product added successfully', 'ok', { duration: 4000 });
    this.syncDataService.backBtn.emit('Home');
    formDirective.resetForm();
    this.productForm.reset();
    this.filtredProduct = this.productForm.get('productName').valueChanges.pipe(startWith(''), map(value => this._filtredProducts(value)));
    this.productForm.get('quantity').valueChanges.subscribe(value => {
      if (value > 0) {
        const amounts = value * this.productForm.value.unitPrice;
        this.productForm.get('amount').setValue(amounts);
      }
    });

    this.productForm.get('unitPrice').valueChanges.subscribe(value => {
      if (value > 0) {
        const amounts = value * this.productForm.value.quantity;
        this.productForm.get('amount').setValue(amounts);
      }
    });
    this.validationQuantity = false;
  }

  onCancel() {
    this.cancel.emit();
    this.syncDataService.backBtn.emit('Home');
  }

}
