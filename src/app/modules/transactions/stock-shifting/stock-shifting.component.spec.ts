import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockShiftingComponent } from './stock-shifting.component';

describe('StockShiftingComponent', () => {
  let component: StockShiftingComponent;
  let fixture: ComponentFixture<StockShiftingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockShiftingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockShiftingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
