import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { fade, expandCollapse } from 'src/app/animations';
import { SyncDataService } from 'src/app/services/sync-data.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-stock-shifting',
  templateUrl: './stock-shifting.component.html',
  styleUrls: ['./stock-shifting.component.scss'],
  animations: [fade, expandCollapse]
})
export class StockShiftingComponent implements OnInit {

  isShiftTo: boolean;
  masterData: any;
  godowns: any[];
  filteredOptions: Observable<any[]>;
  isAddProductForm: boolean = false;
  productData: any[];
  transactions: any;
  shiftingForm = new FormGroup({
    type: new FormControl('TO'),
    entryDate: new FormControl(new Date()),
    godown: new FormControl(''),
    products: new FormArray([]),
    transactionId: new FormControl(''),
    isSyncSuccess: new FormControl(false),
    isSyncFailed: new FormControl(false),
    godownId: new FormControl(2),
    CT: new FormControl('')
  });

  constructor(private syncDataService: SyncDataService,
              private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.isShiftTo = true;
    this.syncDataService.backBtn.subscribe((flag) => {
      if (flag === 'Home') {
        this.isAddProductForm = false;
      }
    });
    if (localStorage.getItem('mulkanoorMasterData')) {
      this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
      this.godowns = this.masterData.outletList;
      this.productData = this.masterData.products;
    }

    if (localStorage.getItem('mulkanoorTransactions')) {
      this.transactions = JSON.parse(localStorage.getItem('mulkanoorTransactions'));
    }

    this.filteredOptions = this.shiftingForm.get('godown').valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  selectedType(type) {
    this.isShiftTo = (type === 'FROM') ? false : true;
    this.shiftingForm.get('type').patchValue(type);
  }

  private _filter(value: string): any[] {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();
      const godown = this.godowns.filter(select => select.outletName.toLowerCase().includes(filterValue));
      return godown;
    }
  }

  displayWith(godown) {
    if (godown) {
        return godown.outletName;
    }
}

  addProductsStock() {
    this.isAddProductForm = !this.isAddProductForm;
    this.syncDataService.backBtn.emit('Add Product');
  }

  addProductsListStock(product) {
    this.shiftingForm.get('products').value.push(product);
    console.log('productList', this.shiftingForm.value.products);
  }

  save() {
    this.shiftingForm.get('transactionId').patchValue((Math.floor((Math.random() * 999999) + 1)).toString());
    this.shiftingForm.get('CT').patchValue(new Date());
    this.transactions.stockShift.push(this.shiftingForm.value);
    localStorage.setItem('mulkanoorTransactions', JSON.stringify(this.transactions));
    this.resetShiftingForm();
    this.shiftingForm.get('entryDate').patchValue(new Date());
    this.snackbar.open('Data saved successfully', 'ok', { duration: 4000 });
    this.filteredOptions = this.shiftingForm.get('godown').valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  resetShiftingForm() {
    this.shiftingForm = new FormGroup({
      type: new FormControl('TO'),
      entryDate: new FormControl(new Date()),
      godown: new FormControl(''),
      products: new FormArray([]),
      transactionId: new FormControl(''),
      isSyncSuccess: new FormControl(false),
      isSyncFailed: new FormControl(false),
      godownId: new FormControl(2),
      CT: new FormControl('')
    });
  }

}
