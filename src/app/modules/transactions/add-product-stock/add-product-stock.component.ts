import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SyncDataService } from 'src/app/services/sync-data.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-add-product-stock',
  templateUrl: './add-product-stock.component.html',
  styleUrls: ['./add-product-stock.component.scss']
})
export class AddProductStockComponent implements OnInit {

  @Output() addProduct = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Input() productData;
  @Input() shiftingType: string;
  unitsList: any[] = [];
  masterData: any;
  filtredProduct: Observable<any[]>;
  productForm = new FormGroup({
    productName: new FormControl('', Validators.required),
    productId: new FormControl(''),
    units: new FormControl('', Validators.required),
    quantity: new FormControl(0, [Validators.required, Validators.min(1)]),
    unitPrice: new FormControl(''),
    amount: new FormControl(''),
    varietyId: new FormControl(''),
    commodityId: new FormControl(''),
    commodityName: new FormControl(''),
    varietyName: new FormControl(''),
    qualifier: new FormControl(''),
    imageUrl: new FormControl(''),
    baseUnitId: new FormControl(''),
    baseUnitName: new FormControl(''),
  });

  constructor(private snackbar: MatSnackBar, private syncDataService: SyncDataService) { }

  ngOnInit() {
    this.filtredProduct = this.productForm.get('productName').valueChanges.pipe(startWith(''), map(value => this._filtredProducts(value)));
  }

  private _filtredProducts(value: string): any[] {
    if (value !== null) {
      const filteredValue = value.toLocaleLowerCase();
      const productSort = this.productData.filter(product => product.productName.toLocaleLowerCase().includes(filteredValue) ||
                                                              product.commodityName.toLocaleLowerCase().includes(filteredValue));
      if (productSort.length) {
        this.productForm.patchValue({
          productId: productSort[0].productId,
          varietyId: productSort[0].varietyId,
          commodityId: productSort[0].commodityId,
          commodityName: productSort[0].commodityName,
          varietyName: productSort[0].varietyName,
          qualifier: '',
          imageUrl: productSort[0].imageUrl,
          baseUnitId: productSort[0].baseUnitId,
          baseUnitName: productSort[0].baseUnitName,
        });
        this.unitsList = productSort[0].listofUnits;
      }
      return productSort;
    }
  }

  submitProduct() {
    this.addProduct.emit(this.productForm.value);
    this.snackbar.open('Product added successfully', 'ok', { duration: 4000 });
    this.syncDataService.backBtn.emit('Home');
    this.productForm.reset();
    this.filtredProduct = this.productForm.get('productName').valueChanges.pipe(startWith(''), map(value => this._filtredProducts(value)));
  }

  onCancel() {
    this.cancel.emit();
    this.syncDataService.backBtn.emit('Home');
  }

}
