import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-transactions',
  templateUrl: './my-transactions.component.html',
  styleUrls: ['./my-transactions.component.scss']
})
export class MyTransactionsComponent implements OnInit {

  constructor() { }
  
  transactions;
  ngOnInit() {
    this.transactions = JSON.parse(localStorage.getItem('transactions'));


  }

}
