import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SyncDataService } from 'src/app/services/sync-data.service';

@Component({
    selector: 'app-add-seller',
    templateUrl: './add-seller.component.html',
    styleUrls: ['./add-seller.component.scss']
})
export class AddSellerComponent implements OnInit {

    @Input() type: any;
    // tslint:disable-next-line: no-output-on-prefix
    @Output() onAdd = new EventEmitter();
    @Output() onCancel = new EventEmitter();

    constructor(private snackbar: MatSnackBar, private syncDataService: SyncDataService) { }

    businessTypes = [
        { value: 'BUSINESS_TYPE', businessTypeId: '', name: 'Business type' },
        { value: 'INTERMEDIARY', businessTypeId: 'BT000013', name: 'Intermediary' },
        { value: 'STUDENT', businessTypeId: 'BT000012', name: 'Student' },
        { value: 'FARMER', businessTypeId: 'BT000002', name: 'Farmer' },
        { value: 'BROKER', businessTypeId: 'BT000001', name: 'Broker(Intermediary)' },
        { value: 'AGRI_TRADER', businessTypeId: 'BT000000', name: 'Agri Trader' },
        { value: 'WAREHOUSER', businessTypeId: 'BT000006', name: 'Warehouser' },
        { value: 'TRANSPORTER', businessTypeId: 'BT000003', name: 'Transporter' },
        { value: 'AGRI_INPUTS', businessTypeId: 'BT000008', name: 'Agri inputs' },
        { value: 'AGRICULTURE__COOPERATIVE_SOCIETY', businessTypeId: 'BT000010', name: 'Agriculture Cooperative Society' },
        { value: 'SUBSCRIPTION_SERVICES', businessTypeId: 'BT000009', name: 'Subscription Services' },
        { value: 'PROCESSOR', businessTypeId: 'BT000007', name: 'Processor' },
    ];



    sellerForm = new FormGroup({
        businessType: new FormControl('', Validators.required),
        businessName: new FormControl(''),
        firstName: new FormControl('', Validators.required),
        mobileNo: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{10,10}$')]),
        businessTypeId: new FormControl(''),
        listOfPlaces: new FormControl(''),
        createdByProfileKey: new FormControl(''),
        createdByBusinessKey: new FormControl('')
    });

    resetSellerForm() {
        
        // this.sellerForm.setValue({
        //     businessType: '',
        //     businessName: '',
        //     firstName: '',
        //     mobileNo: '0',
        //     businessTypeId: '',
        //     listOfPlaces: '',
        //     createdByProfileKey: '',
        //     createdByBusinessKey: '',
        // })
    }

    businessContactList = [];
    masterData = {
        connects: [],
        bizContacts: []
    };

    ngOnInit() {}

    isAlreadyRegistered(existingContacts): boolean {
        for (let i = 0; i < existingContacts.length; i++) {
            if (existingContacts[i].mobileNo === this.sellerForm.value.mobileNo || existingContacts[i].loginId === this.sellerForm.value.mobileNo) {
                return true;
            }
        }

        return false;
    }

    submit(formDirective: FormGroupDirective) {

        let businessContact = this.sellerForm.value;

        // Get Business type id and set to business contact
        this.sellerForm.value.businessTypeId = this.businessTypes.find((item) => (item.value === businessContact.businessType)).businessTypeId;
        this.sellerForm.value.listOfPlaces = [];
        this.sellerForm.value.loginId = this.sellerForm.value.mobileNo;
        // Get the location details for the business contact from local storage and set
        if (localStorage.getItem("userData")) {
            var userData = JSON.parse(localStorage.getItem("userData"));
            this.sellerForm.value.createdByProfileKey = userData.profileKey;
            this.sellerForm.value.createdByBusinessKey = userData.lstOfBusinessInfo[0].businessKey;
        }


        // Store business contact in local storage

        if (localStorage.getItem("businessContacts") && JSON.parse(localStorage.getItem("businessContacts")).length > 0) {
            let existingContacts = JSON.parse(localStorage.getItem("businessContacts"));


            this.masterData = JSON.parse(localStorage.getItem("masterData"));
            existingContacts = existingContacts.concat(this.masterData.connects);
            existingContacts = existingContacts.concat(this.masterData.bizContacts);
            if (!this.isAlreadyRegistered(existingContacts)) {
                this.businessContactList.push(this.sellerForm.value);
                // this.businessContactObj.bizContancts=this.businessContactList;

                localStorage.setItem("businessContacts", JSON.stringify(this.businessContactList));
                //   localStorage.setItem("businessContacts",JSON.stringify(this.businessContactObj));

            } else {
                this.snackbar.open('User Already Registered', 'OK', { duration: 4000 });
                return;
            }
        }
        else {

            this.businessContactList.push(this.sellerForm.value);
            // this.businessContactObj.bizContancts=this.businessContactList;

            localStorage.setItem("businessContacts", JSON.stringify(this.businessContactList));
            // localStorage.setItem("businessContacts",JSON.stringify(this.businessContactObj));

        }


        this.onAdd.emit(this.sellerForm.value);
        const user = this.type === 'SALE' ? 'Buyer ' : 'Seller';
        this.snackbar.open(user + ' added successfully', 'ok', { duration: 4000 });
        this.syncDataService.backBtn.emit('Home');
        formDirective.resetForm();
        this.sellerForm.reset();
    }



    cancel() {
        this.onCancel.emit();
        this.syncDataService.backBtn.emit('Home');
    }

}
