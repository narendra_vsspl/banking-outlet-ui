import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockboardComponent } from './stockboard.component';

describe('StockboardComponent', () => {
  let component: StockboardComponent;
  let fixture: ComponentFixture<StockboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
