import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from '../../auth/authentication.service';
import { SyncDataService } from 'src/app/services/sync-data.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-stockboard',
    templateUrl: './stockboard.component.html',
    styleUrls: ['./stockboard.component.scss']
})
export class StockboardComponent implements OnInit {

    constructor(private snackbar: MatSnackBar,
        private authService: AuthenticationService,
        private syncDataService: SyncDataService,
        private spinnerService: SpinnerService, ) { }

    masterData: any;
    stockboardData: any[];
    model: any;
    user: any;
    transactionTypes: any = [{ value: 'SALE', name: 'SALE' }, { value: 'PURCHASE', name: 'PURCHASE' }];
    connects: any[] = [];
    isTypeChange: boolean = true;
    filteredOptions: Observable<any[]>;
    receiverData: any;
    productData: any[] = [];
    isReceiver: boolean = false;
    isSellerForm: boolean = false;
    isAddProductForm: boolean = false;
    lastSyncTime: any;
    ngOnInit() {
        this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));

        if (this.masterData && this.masterData.stockData) {
            this.stockboardData = new Array();
            for (let i in this.masterData.stockData) {
                this.stockboardData.push(this.masterData.stockData[i]);
            }
        }
        // this.syncMasterData();
    }
    syncMasterData() {
        this.user = this.authService.getUserData();

        this.authService.getOutletMasterData().subscribe(res => {
            if (res.code === 200) {
                localStorage.setItem('mulkanoorMasterData', JSON.stringify(res.data));
                this.masterData = res.data;
                this.connects = this.masterData.connects;
                this.connects = this.connects.concat(this.masterData.bizContacts);
                this.productData = this.masterData.products;
                this.lastSyncTime = this.masterData.lastMasterSyncTs;
                this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));

                if (this.masterData && this.masterData.stockData) {
                    this.stockboardData = new Array();
                    // tslint:disable-next-line: forin
                    for (let i in this.masterData.stockData) {
                        this.stockboardData.push(this.masterData.stockData[i]);
                    }
                }

                this.snackbar.open('Data fetch successfully', 'ok', { duration: 4000 });
            } else {
                this.snackbar.open('Data sync failed', 'ok', { duration: 4000 });
            }
        });

    }
}
