import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from '../../auth/authentication.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Profile } from 'selenium-webdriver/firefox';
import { fade, expandCollapse } from 'src/app/animations';
import { SyncDataService } from 'src/app/services/sync-data.service';
import { SpinnerService } from 'src/app/services/spinner.service';


export interface Profile {
    firstName: string;
    deleted: boolean;
    profilePicURL: string;
    businessTypeName: string;
    businessName: string;
    businessId: string;
    placeTo: any;
    currencyCode: string;
    userId: string;
    mobileContact: boolean;
    businessTypeId: string;
    TS: string;

}

@Component({
    selector: 'app-create-transaction',
    templateUrl: './create-transaction.component.html',
    styleUrls: ['./create-transaction.component.scss'],
    animations: [fade, expandCollapse]
})
export class CreateTransactionComponent implements OnInit {

    model: any;
    user: any;
    masterData: any;
    transactionTypes: any = [{ value: 'SALE', name: 'SALE' }, { value: 'PURCHASE', name: 'PURCHASE' }];
    connects: any[] = [];
    isTypeChange: boolean = true;
    filteredOptions: Observable<any[]>;
    receiverData: any;
    productData: any[] = [];
    isReceiver: boolean = false;
    isSellerForm: boolean = false;
    isAddProductForm: boolean = false;
    lastSyncTime: any;
    isDeductCharges: boolean = false;
    transactionObj: any = {
        transactionId: '',
        productList: [],
        chargesList: [],
        initiatorProfileKey: '',
        hammaliCharges: 0,
        transportCharges: 0,
        receiverFirstName: '',
        receiverId: '',
        receiverProfileKey: '',
        receiverBusinessTypeId: '',
        transactionType: '',
        receiverLocation: '',
        totalAmount: '',
        entryDate: '',
        isSyncSuccess: false,
        isSyncFailed: false,
        CT: new Date(),
        godownId: 2
    };

    transactionForm = new FormGroup({
        type: new FormControl('SALE'),
        date: new FormControl(new Date()),
        userCtrl: new FormControl(),
        receiverName: new FormControl(''),
        chargeType: new FormControl(''),
        chargeAmount: new FormControl(0.0),
        mobileNo: new FormControl(''),
    });

    isProductedSelected = false;
    isSeller = false;
    transactionList = [];
    transactions = {
        transactions: [],
        outletProfileKey: 0,
        stockShift: []
    };

    chargeTypes = [
        { value: 'HAMMALI', name: 'Hamali' },
        { value: 'TRANSPORTATION', name: 'Transportation' },
    ];
    charges: any[];

    productList: any;
    stockData: any;
    keys: any;
    transactionType: any;
    newStockData = {
        closingBalance: 0,
        productId: '',
        productName: '',
        purchaseQty: 0,
        saleQty: 0,
        openingbalance: 0
    };

    constructor(
        private snackbar: MatSnackBar,
        private authService: AuthenticationService,
        private syncDataService: SyncDataService,
        private spinnerService: SpinnerService) { }


    ngOnInit() {
        this.user = this.authService.getUserData();
        this.syncDataService.backBtn.subscribe((flag) => {
            if (flag === 'Home') {
                this.isAddProductForm = false;
                this.isSellerForm = false;
            }
        });
        if (this.user) {
            this.authService.getOutletMasterData().subscribe(res => {
                if (res.code === 200) {
                    localStorage.setItem('mulkanoorMasterData', JSON.stringify(res.data));
                    this.masterData = res.data;
                    this.connects = this.masterData.connects;
                    // this.connects = this.connects.concat(this.masterData.bizContacts);
                    this.charges = this.masterData.charges;
                    this.productData = this.masterData.products;
                    this.lastSyncTime = this.masterData.lastMasterSyncTs;
                    this.filteredOptions = this.transactionForm.get('userCtrl').valueChanges
                        .pipe(
                            startWith(''),
                            map(value => this._filter(value))
                        );
                } else {
                    this.snackbar.open('Unable to fetch the master data, Please login after some time', 'ok', { duration: 4000 });
                }

            },
                (err => {
                    this.snackbar.open('Internal Server Error, please check your internet connection', 'X', { duration: 4000 });
                    this.spinnerService.hide();
                }));
        }

        if (localStorage.getItem('mulkanoorMasterData')) {
            this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
            this.connects = this.masterData.connects;
            this.productData = this.masterData.products;
        }


        const initTransactionsObject = {
            transactions: [],
            outletProfileKey: 2,
            stockShift: []
        };

        if (!localStorage.getItem('mulkanoorTransactions')) {
            localStorage.setItem('mulkanoorTransactions', JSON.stringify(initTransactionsObject));
        }

        this.filteredOptions = this.transactionForm.get('userCtrl').valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );
    }

    get totalAmount() {
        const total = (this.transactionObj.productList || []).reduce((a, i) => {
            return a + (+i.amount);
        }, 0);
        return total + (+this.transactionObj.hammaliCharges) + (+this.transactionObj.transportCharges);
    }

    private _filter(value: string): any[] {
        if (typeof value === 'string') {
            const filterValue = value.toLowerCase();
            const receiver = this.connects.filter(connect => connect.firstName.toLowerCase().includes(filterValue) || connect.userId.toLowerCase().includes(filterValue));
            return receiver;
        }
    }

    displayWith(receiver) {
        if (receiver) {
            return receiver.firstName + ' (' + receiver.userId + ')';
        }
    }

    addProducts() {
        this.isAddProductForm = !this.isAddProductForm;
        this.syncDataService.backBtn.emit('Add Product');
    }

    addCharges(charge) {
        const type = this.transactionForm.get('chargeType').value;
        const value = this.transactionForm.get('chargeAmount').value;
        if (type === 'HAMMALI') {
            this.transactionObj.hammaliCharges = (charge === 'add') ? value : -parseFloat(value);
            this.transactionObj.chargesList.push({ chargeType: this.charges[1], value: this.transactionObj.hammaliCharges });
        } else if (type === 'TRANSPORTATION') {
            this.transactionObj.transportCharges = (charge === 'add') ? value : -parseFloat(value);
            this.transactionObj.chargesList.push({ chargeType: this.charges[0], value: this.transactionObj.transportCharges });
        }

        this.transactionForm.get('chargeType').reset();
        this.transactionForm.get('chargeAmount').reset();
    }

    selectedType(type) {
        this.isSeller = (type === 'PURCHASE') ? true : false;
        this.transactionForm.get('type').patchValue(type);
    }

    saveOffline() {
        this.transactionObj.transactionId = (Math.floor((Math.random() * 999999) + 1)).toString();
        this.transactionObj.initiatorProfileKey = this.user.profileKey;
        this.transactionObj.receiverFirstName = this.transactionForm.value.userCtrl.firstName;
        this.transactionObj.receiverMobileNo = this.transactionForm.value.userCtrl.loginId;
        this.transactionObj.receiverId = this.transactionForm.value.userCtrl.userId || this.user.profileKey;
        this.transactionObj.receiverProfileKey = this.transactionForm.value.userCtrl.userId || this.user.profileKey;
        this.transactionObj.receiverLocation = this.transactionForm.value.userCtrl.placeTo ?
            this.transactionForm.value.userCtrl.placeTo.locationLong :
            this.user.lstOfBusinessInfo[0].objBusinessCity.locationShort;
        this.transactionObj.receiverBusinessTypeId = this.transactionForm.value.userCtrl.businessTypeId;
        this.transactionObj.transactionType = this.transactionForm.value.type === 'SALE' ? 3 : 4;
        this.transactionObj.entryDate = this.transactionForm.value.date;
        this.transactionObj.totalAmount = this.totalAmount;
        this.transactionObj.CT = new Date();
        // this.transactionObj.profilePicURL = this.transactionForm.value.userCtrl.profilePicURL;

        if (!localStorage.getItem('mulkanoorTransactions')) {
            localStorage.setItem('mulkanoorTransactions', JSON.stringify(this.transactions));
        }

        this.transactions = JSON.parse(localStorage.getItem('mulkanoorTransactions'));
        this.transactionList = this.transactions.transactions;
        this.transactionList.push(this.transactionObj);
        this.transactions.transactions = this.transactionList;
        this.transactions.outletProfileKey = 2;
        localStorage.setItem('mulkanoorTransactions', JSON.stringify(this.transactions));
        //update stock table
        this.updateStockTable(this.transactionObj.productList, this.transactionObj.transactionType);
        this.clearTransactionObj();
        this.snackbar.open('Data saved successfully', 'ok', { duration: 4000 });
        this.filteredOptions = this.transactionForm.get('userCtrl').valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );

    }

    clearTransactionObj() {
        this.transactionObj = {
            transactionId: '',
            productList: [],
            chargesList: [],
            initiatorProfileKey: '',
            hammaliCharges: 0,
            transportCharges: 0,
            receiverFirstName: '',
            receiverId: '',
            receiverProfileKey: '',
            receiverBusinessTypeId: '',
            transactionType: '',
            receiverLocation: '',
            totalAmount: '',
            entryDate: '',
            isSyncSuccess: false,
            isSyncFailed: false,
            CT: new Date(),
            godownId: 2
        };

        this.transactionForm = new FormGroup({
            type: new FormControl('SALE'),
            date: new FormControl(new Date()),
            userCtrl: new FormControl(),
            receiverName: new FormControl(''),
            chargeType: new FormControl(''),
            chargeAmount: new FormControl(0.0)
        });
    }

    toggleAddSeller() {
        this.isSellerForm = !this.isSellerForm;
        this.syncDataService.backBtn.emit(this.isSeller ? 'Create Seller' : ' Create Buyer');
    }

    addSellerData(receiver) {
        this.isSellerForm = false;
        this.transactionForm.get('userCtrl').patchValue(receiver);
    }

    addProductsList(product) {
        this.isAddProductForm = false;
        this.transactionObj.productList.push(product);
    }

    afterSync() {

        if (localStorage.getItem('mulkanoorMasterData')) {
            this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
            this.connects = this.masterData.connects;
            this.productData = this.masterData.products;
        }
        this.filteredOptions = this.transactionForm.get('userCtrl').valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );
    }

    updateStockTable(productList, transactionType) {
        // this.transactions = JSON.parse(localStorage.getItem('mulkanoorTransactions'));
        this.masterData = JSON.parse(localStorage.getItem('mulkanoorMasterData'));
        this.stockData = this.masterData.stockData;
        this.productList = productList;
        this.transactionType = transactionType;
        this.keys = Object.keys(this.stockData);
        // console.log(this.keys);
        this.productList.forEach(element => {
            if (element.baseUnitName !== element.units.unitName) {
                element.quantity = (element.units.conversion * element.quantity);
            }
            if (this.stockData[element.productId]) {
                if (this.transactionType === 'SALE') {
                    // console.log(this.stockData[element.productId]);
                    this.stockData[element.productId].openingbalance -= element.quantity;
                    this.stockData[element.productId].closingBalance -= element.quantity;
                    this.stockData[element.productId].saleQty += element.quantity;
                    this.masterData.stockData = this.stockData;
                    localStorage.setItem('mulkanoorMasterData', JSON.stringify(this.masterData));

                }
                if (this.transactionType === 'PURCHASE') {
                    this.stockData[element.productId].openingbalance += element.quantity;
                    this.stockData[element.productId].closingBalance += element.quantity;
                    this.stockData[element.productId].purchaseQty += element.quantity;
                    this.masterData.stockData = this.stockData;
                    localStorage.setItem('mulkanoorMasterData', JSON.stringify(this.masterData));
                }
            } else {
                if (this.transactionType === 'SALE') {
                    this.newStockData.closingBalance -= element.quantity;
                    this.newStockData.openingbalance -= element.quantity;
                    this.newStockData.productId = element.productId;
                    this.newStockData.productName = element.productName;
                    this.newStockData.saleQty += element.quantity;
                    this.stockData[element.productId] = this.newStockData;
                    this.masterData.stockData = this.stockData;
                    localStorage.setItem('mulkanoorMasterData', JSON.stringify(this.masterData));
                }
                if (this.transactionType === 'PURCHASE') {
                    this.newStockData.closingBalance += element.quantity;
                    this.newStockData.openingbalance += element.quantity;
                    this.newStockData.productId = element.productId;
                    this.newStockData.productName = element.productName;
                    this.newStockData.purchaseQty += element.quantity;
                    this.stockData[element.productId] = this.newStockData;
                    this.masterData.stockData = this.stockData;
                    localStorage.setItem('mulkanoorMasterData', JSON.stringify(this.masterData));
                }
            }

        });

    }
}
