export const environment = {
  production: true,
  domain     : 'https://kalgudi.com',
  restBaseUrl: 'https://kalgudi.com/rest/v1',
  restBaseUrlV2: 'https://kalgudi.com/v2',
  ouletProfileKey: 'M01mst90PRFREG2019092421496640UNH001'
};
